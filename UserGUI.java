import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class UserGUI extends Application {
	
	private TextField firstName = new TextField();
	private TextField lastName = new TextField();
	private TextField userName = new TextField();
	private TextField passWord = new TextField();
	private TextField gradeLevel = new TextField();
	private Button submit = new Button("Submit");
	
	@Override //Override the start method in the Application class
	public void start(Stage primaryStage) {
		
		//Create UI
		
		GridPane gridPane = new GridPane();
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		gridPane.add(new Label("First Name:"), 0, 0);
		gridPane.add(firstName, 1, 0);
		gridPane.add(new Label("Last Name:"), 0, 1);
		gridPane.add(lastName, 1, 1);
		gridPane.add(new Label("User Name:"), 0, 2);
		gridPane.add(userName, 1, 2);
		gridPane.add(new Label("Password:"), 0, 3);
		gridPane.add(passWord, 1, 3);
		gridPane.add(new Label("Grade Level:"), 0, 4);
		gridPane.add(gradeLevel, 1, 4);
		gridPane.add(submit, 1, 5);
		
		  //Set properties for UI
		  gridPane.setAlignment(Pos.CENTER);
		  firstName.setAlignment(Pos.BOTTOM_RIGHT);
		  lastName.setAlignment(Pos.BOTTOM_RIGHT);
		  userName.setAlignment(Pos.BOTTOM_RIGHT);
		  passWord.setAlignment(Pos.BOTTOM_RIGHT);
		  gradeLevel.setAlignment(Pos.BOTTOM_RIGHT);
		  
		  //process event 
		  submit.setOnAction(e -> displayAccountCreated());
		  
		// Create a scene and place it in the stage
	       Scene scene = new Scene(gridPane, 400, 250);
		   primaryStage.setTitle("LoanCalculator"); // Set title
		   primaryStage.setScene(scene); // Place the scene in the stage
		   primaryStage.show(); // Display the stage
		  }
		
		
			public void displayAccountCreated() {
				System.out.println("Welcome" + this.firstName + "to the Student Planner Database!" );
			}

	
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	

}