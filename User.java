
public class User {
	
	private String firstName;
	private String lastName;
	private String userName; 
	private String password;
	private int gradeLevel;
	
	
	public User(String fName, String lName, String uName, String pword, int grade) {
		
		this.firstName = fName;
		this.lastName = lName;
		this.userName = uName;
		this.password = pword;
		this.gradeLevel = grade;
		
	}
	


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public int getGradeLevel() {
		return gradeLevel;
	}


	public void setGradeLevel(int gradeLevel) {
		this.gradeLevel = gradeLevel;
	}
	
	
	
}